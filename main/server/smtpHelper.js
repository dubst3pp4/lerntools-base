//External libs
const nodemailer=require('nodemailer');
const debug=require('debug')('helpers');

//Internal libs
const config=__config;
const consts=require('../../consts.js');

//Database schemas
const User=require('./modelUser.js');

//Check if test is a valid email address
exports.isValidMail=function(test) {
	if (!test) return false;
	return test.match(consts.REGEX_EMAIL);
}

/*
 Send info mail to registered user
 to: either email-address or registered user
 subject: subject email
 html: text email
 */
exports.sendInfoMail=function(to, subject, html) {
	if (!to) return;
	if (!subject) subject="no subject";
	if (!html) html="";
	var from=config.INFO_MAIL;
	//valid email address?
	if (to.match(consts.REGEX_EMAIL)) {
		return sendMail(from, to, subject, html);
	}
	//regsitered user?
	User.findOne({ login: to }, function (err, user) {
		if (err) debug(err);
		if (user) {
			sendMail(from, user.email, subject, html);
		}
		else debug('Couldn\'t send email to: '+to)
	})
}

/*
 Use nodemailer for sending emails, internal function
 */
function sendMail(from, to, subject, html) {
	var transporter=nodemailer.createTransport(config.SMTP_CONF);
	var mailOptions={
		from: from,
		to: to,
		subject: subject,
		html: html
	};
	transporter.sendMail(mailOptions, (error, info)=> {
		if (error) debug(error);
		else debug('Sent email to: '+to);
	});
}
