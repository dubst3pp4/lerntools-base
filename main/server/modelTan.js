var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var TanSchema=new Schema({
	login:		{type: String, required: true, max: 100},
	code:		{type: String, required: true, max: 100},
	expires:	{type: Date, required: true}
});


module.exports=mongoose.model('Tan', TanSchema );
