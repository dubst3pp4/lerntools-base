var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var CaptchaSchema=new Schema({
	text:		{type: String, required: true, max: 100},
	ticket:		{type: String, unique:true, required: true},
	created:	{type: Date, required: true}
});


module.exports=mongoose.model('Captcha', CaptchaSchema );
