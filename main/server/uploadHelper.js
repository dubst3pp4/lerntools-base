//External libs
const debug=require('debug')('helpers');
const path=require('path');
const fs=require('fs');
const mime=require('mime');
const md5=require('md5');

//Internal libs
const config=__config;
const consts=require ('../../consts.js');

/*
Store content of base64 encoded url in upload console.dir
url: Base64 encoded url
fileType: Ending (.img, .wav etc.)
moduleName: module name
returns: uploadName (consists of md5 hash + extension according to mimetype)
*/
exports.addBase64Upload=function(base64url,moduleName) {
	var uploadFileName='';
	if (!config.UPLOAD_DIR) return '';
	try {
		var uploadDir=getUploadDir(moduleName);
		//parse url
		var parts = base64url.match(/^data:([A-Za-z0-9-+\/]+);base64,(.+)$/);
		if (!parts || parts.length!=3) return '';	//validation error
		//check mimetype and set extension
		var mimeType=parts[1];
		if (!mimeType || !consts.UPLOAD_MIME_TYPES.indexOf(mimeType)<0) return ''; //unsupported mimeType
		var extension=mime.getExtension(mimeType);
		//check mimetype
		var base64Data=parts[2];
		uploadName=md5(base64Data)+'.'+extension;
		var fileName=path.join(uploadDir,uploadName);
		debug('Saving upload' +fileName);
		fs.writeFileSync(fileName, base64Data, 'base64');
	}
	catch (err) {
		debug(err);
		uploadName='';
		debug('Could not save upload');
	}
	return uploadName;
}

/*
Check if given uploadName exists as file
*/
exports.uploadExists=function(uploadName,moduleName) {
	try {
		var uploadDir=getUploadDir(moduleName);
		var fileName=path.join(uploadDir,uploadName);
		return fs.existsSync(fileName);
	}
	catch (err) {
		debug(err);
		return false;
	}
}

/*
return mime type oft base64 encoded URL
*/
exports.getMimeTime=function(base64url) {
	var parts = base64url.match(/^data:([A-Za-z-+\/]+);base64,.+$/);
	var mime=parts[1];
	return mime;
}

/*
Delete file from upload console.dir
moduleName: name of module
*/
exports.delUpload=function(uploadName, moduleName) {
	if (!uploadName) return;
	try {
		var uploadDir=getUploadDir(moduleName);
		var fileName=path.join(uploadDir,uploadName);
		debug('Deleting upload' +fileName);
		fs.unlinkSync(fileName);
	}
	catch (err) {
		debug(err);
		console.log('Could not delete upload');
	}
}

/*
Clean all uploads not contained in list
*/
exports.cleanOrphanedUploads=function(uploadNames, moduleName) {
	try {
		var uploadDir=getUploadDir(moduleName);
		fs.readdir(uploadDir, (err, files) => {
			files.forEach(file => {
				if (uploadNames.indexOf(file)<0) {
					debug('Removing orphaned upload '+file);
					var fileName=path.join(uploadDir,file);
					fs.unlinkSync(fileName);
				}
			});
		});
	}
	catch (err) {
		debug(err);
		console.log('Could not clean orphaned upload');
	}
}

/*
Returns full path of upload dir (and creates dir if not existing)
moduleName: name of module
*/
function getUploadDir(moduleName) {
	/*Ensure upload dir exists*/
	var uploadDir=path.join(__basedir,config.UPLOAD_DIR,moduleName);
	if (!fs.existsSync(uploadDir)) {
		debug('Creating uploadDir '+uploadDir);
		fs.mkdirSync(uploadDir);
	}
	return uploadDir;
}
