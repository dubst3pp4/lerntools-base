export default {
	downloadJson: function (o) {
		var data=JSON.stringify(o);
		var blob=new Blob([data], {type: 'text/plain'});
		var e=document.createEvent('MouseEvents'), a=document.createElement('a');
		if (o.title) a.download=o.title+'.json';
		else if (o.name) a.download=o.name+'.json';
		else a.download="download.json";
		a.href=window.URL.createObjectURL(blob); a.dataset.downloadurl=['text/json', a.download, a.href].join(':');
		e.initEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
		a.dispatchEvent(e);
	},

	downloadHtml: function (title, html) {
		var blob=new Blob([html], {type: 'text/plain'});
		var e=document.createEvent('MouseEvents'), a=document.createElement('a');
		a.download=title+'.html';
		a.href=window.URL.createObjectURL(blob);
		a.dataset.downloadurl=['text/html', a.download, a.href].join(':');
		e.initEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
		a.dispatchEvent(e);
	},

	downloadText: function (title, text) {
		var blob=new Blob([text], {type: 'text/plain'});
		var e=document.createEvent('MouseEvents'), a=document.createElement('a');
		a.download=title+'.txt';
		a.href=window.URL.createObjectURL(blob);
		a.dataset.downloadurl=['text/plain', a.download, a.href].join(':');
		e.initEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
		a.dispatchEvent(e);
	}
}
