//Regular expression for media types
const REGEX_IMAGE_URL= /\.(png|jpe?g|gif|svg|bmp)/i
const REGEX_AUDIO_URL=/\.(mp3|wav|ogg)/i
const REGEX_MOVIE_URL=/\.(mpe?g|mkv|mp4|avi|mov|mpv|wmf|webm)/i

import consts from '../../consts.js'

export default {
	//Escape HTML
	escapeHTML: function (string) {
		var temp=String(string).replace(/[&<>"'`=\/]/g, function (s) {
			return consts.HTML_ENTITY_MAP[s];
		});
		return temp.replace(/\n/g, "<br>");
	},

	/*Format urls inside normal text
	no longer required due to markdown
	convertUrls: function (text) {
		if (!text) return "";
		var urlPattern=/(https:|http:|www.)[^ ]{1,1000}/
		return text.replace(urlPattern, function(match, offset, string) {
			var href=match;
			var name=match;
			//Complete protocol
			if (href.startsWith('www')) href='http://'+href;
			//Remove protocol for display
			if (name.startsWith('https:&#x2F;&#x2F;')) name=name.substring(18);
			if (name.startsWith('http:&#x2F;&#x2F;')) name=name.substring(17);
			//Image
			if (href.match(REGEX_IMAGE_URL)) {
				return '<img style="max-height:200px;max-width:100%" src="'+href+'" alt="'+name+'" onclick="location.href=\''+href+'\'">';
			}
			//Audio
			else if (href.match(REGEX_AUDIO_URL)) {
				return '<audio controls style="width:250px"><source src="'+href+'"></audio>';
			}
			//Video
			else if (href.match(REGEX_MOVIE_URL)) {
				return '<video width="320" height="240" controls><source src="'+href+'"></video>';
			}
			//Normal link
			else {
				return '<a href="'+href+'">'+name+'</a>';
			}
		});
	}*/
}
